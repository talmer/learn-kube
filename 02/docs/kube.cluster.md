###### Компоненты кластера Kubernetes:

* [kubelet](https://kubernetes.io/docs/reference/command-line-tools-reference/kubelet/) \- агент управляющий подами\.
* [kube-apiserver](https://kubernetes.io/docs/reference/command-line-tools-reference/kube-apiserver/) \- обрабатывает API запросы\.
* [kube-controller-manager](https://kubernetes.io/docs/reference/command-line-tools-reference/kube-controller-manager/) \- запускает процессы контроллера кластера\.
* [kube-proxy](https://kubernetes.io/docs/reference/command-line-tools-reference/kube-proxy/) \- реализует собой Service\, управляет перенаправлением траффика\.
* [kube-scheduler](https://kubernetes.io/docs/reference/command-line-tools-reference/kube-scheduler/) \- распределяет поды по нодам кластера\.
* [etcd](https://etcd.io/docs/) \- База данных кластера\.

- - -

![компоненты кластера](https://d33wubrfki0l68.cloudfront.net/7016517375d10c702489167e704dcb99e570df85/7bb53/images/docs/components-of-kubernetes.png)

- - -

**Типы нод:**

* Control plane - контроллеры кластера.
* Worker - рабочие ноды.

- - -

<br>
<br>
