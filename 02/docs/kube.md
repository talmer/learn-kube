###### Базовые сущности:

* [Pod](https://kubernetes.io/docs/concepts/workloads/pods/)
* [ReplicaSet](https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/)
* [Deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/)
* [DaemonSet](https://kubernetes.io/docs/concepts/workloads/controllers/daemonset/)
* [StatefulSet](https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/)
* [Job](https://kubernetes.io/docs/concepts/workloads/controllers/job/)
* [CronJob](https://kubernetes.io/docs/concepts/workloads/controllers/cron-jobs/)

- - -

###### Структура Deployment:

* Deployment
    * ReplicaSet
        * Pod
            * containers

- - -

<br>
<br>
