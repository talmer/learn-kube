Варианты компактных дистрибутивов Kubernetes для разработки (и не только):

- [minikube](https://kubernetes.io/ru/docs/tasks/tools/install-minikube/)
- [microk8s](https://microk8s.io/)
- [k3s](https://k3s.io/)
- [minishift](https://www.okd.io/minishift/)
- [kind](https://github.com/kubernetes-sigs/kind)
- [docker-desktop (kubernetes)](https://docs.docker.com/docker-for-windows/#kubernetes)
- [rancher](https://rancher.com/docs/rancher/v2.5/en/quick-start-guide/deployment/quickstart-manual-setup/)

и тд и тп ...
