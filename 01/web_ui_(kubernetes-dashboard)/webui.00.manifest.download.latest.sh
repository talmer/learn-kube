github_get_latest_release() {
    latest=$(curl -s https://api.github.com/repos/${1}/releases/latest | grep 'tag_name' | cut -d\" -f4)
    echo "${latest}"
}

ver=$(github_get_latest_release kubernetes/dashboard)

echo "Latest: ${ver}"

echo "WEBUI_VERSION=${ver}" > ./.env

curl -L https://raw.githubusercontent.com/kubernetes/dashboard/${ver}/aio/deploy/recommended.yaml \
    -o kubernetes.dashboard.${ver}.yaml


